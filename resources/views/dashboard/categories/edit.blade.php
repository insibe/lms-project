@extends('layouts.dashboard')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">{{ $page_title }}</h1>

        </div>
        <!-- DataTales Example -->
        <!-- DataTales Example -->
        <div class="row  ">
               <div class="col-lg-8">
                   <div class="card shadow mb-4">
                       {!! Form::model($category, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url  ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                       <div class="card-body">

                           <div class="form-group">
                               <label class="form-label" for="full-name">Category Title</label>

                               {!! Form::text('title',null, ['class' => 'form-control ', 'placeholder'=>'Enter Category Title','required' =>'required']) !!}

                           </div>

                           <button type="reset" class="btn btn-secondary " >Reset</button>
                           <button type="submit" class="btn btn-primary" >Save</button>



                       </div>
                       {!! Form::close() !!}
                   </div>





               </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
