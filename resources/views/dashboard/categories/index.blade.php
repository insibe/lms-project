@extends('layouts.dashboard')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">{{ $page_title }}</h1>
            <a href="{{ url('/dashboard/categories/create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i>Create Category</a>
        </div>
        <!-- Page Heading -->


        <!-- DataTales Example -->
        <div class="row">
          <div class="col-12">
              <div class="card shadow mb-4">

                  <div class="card-body">
                      <div class="table-responsive">
                          <table class="table">
                              <thead>
                              <tr>
                                  <th scope="col">Category</th>
                                  <th scope="col">slug</th>
                                  <th scope="col">Date Created</th>
                                  <th scope="col"></th>
                              </tr>
                              </thead>
                              <tbody>
                              @if(count($categories ) > 0)
                                  @foreach($categories as $category)
                                      <tr>
                                          <td>{{ $category->title }}</td>
                                          <td>{{ $category->slug }}</td>
                                          <td>{{ $category->created_at }}</td>
                                          <td> <form action="{{url('dashboard/categories/'.$category->id)}}" method="post">
                                                  @method('DELETE')
                                                  @csrf
                                                  <button type="submit">Delete</button>
                                              </form>  </td>
                                          <td> <a class="btn btn-sm btn-primary" href="{{ url('dashboard/categories/'.$category->id.'/edit') }}">Edit</a> </td>
                                      </tr>
                                  @endforeach
                              @else

                              @endif
                              </tbody>
                          </table>
                          {{ $categories->links() }}
                      </div>
                  </div>
              </div>
          </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
