@extends('layouts.dashboard')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">{{ $page_title }}</h1>
            <a href="{{ url('/dashboard/courses/lessons/create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i>Create a  New Lesson</a>
        </div>
        <div class="card shadow mb-4">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Lesson Title</th>
                            <th scope="col">Course Title</th>
                            <th scope="col">Created At</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(count($lessons ) > 0)
                            @foreach($lessons as $lesson)
                        <tr>
                            <td>{{ $lesson->title }}</td>
                            <td>{{ $lesson->course_id }}</td>
                            <td>{{ $lesson->created_at }}</td>
                            <td> <a class="btn btn-sm btn-primary" href="{{ url('dashboard/courses/lessons/'.$lesson->id.'/edit') }}">Edit</a> </td>
                        </tr>
                            @endforeach
                        @else

                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
