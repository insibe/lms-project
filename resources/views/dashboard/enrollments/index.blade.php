@extends('layouts.dashboard')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">{{ $page_title }}</h1>

        </div>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">User</th>
                            <th scope="col">Course</th>
                            <th scope="col">Course Enrolled</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if(count($enrollments) > 0)
                            @foreach($enrollments as $enrollment)
                        <tr>

                            <td>{{ $enrollment->user['name']   }}</td>
                            <td>{{ $enrollment->course['title'] }}</td>
                            <td>{{ $enrollment->created_at }}</td>

                        </tr>
                            @endforeach
                        @else

                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
