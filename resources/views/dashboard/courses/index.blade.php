@extends('layouts.dashboard')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">{{ $page_title }}</h1>
            <a href="{{ url('/dashboard/courses/create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i>Create New Course</a>
        </div>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Course Title</th>
                            <th scope="col">Language</th>
                            <th scope="col">Price</th>
                            <th scope="col">Created At</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($courses ) > 0)
                            @foreach($courses as $course)
                        <tr>
                            <td>{{ $course->title }}</td>
                            <td>{{ $course->language }}</td>
                            <td>{{ $course->price }}</td>
                            <td>{{ $course->created_at }}</td>
                            <td> <a class="btn btn-sm btn-primary" href="{{ url('dashboard/courses/'.$course->id.'/edit') }}">Edit</a> </td>
                        </tr>
                            @endforeach
                        @else

                        @endif
                        </tbody>
                    </table>
                    {{ $courses->links() }}
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
