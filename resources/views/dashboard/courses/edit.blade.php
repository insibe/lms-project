@extends('layouts.dashboard')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">{{ $page_title }}</h1>

        </div>
        <!-- DataTales Example -->
        <!-- DataTales Example -->
        <div class="row  ">
               <div class="col-lg-8">
                   <div class="card shadow mb-4">
                       {!! Form::model($course, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url  ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                       <div class="card-body">

                           <div class="form-group">
                               <label class="form-label" for="full-name">Course Category</label>
                               {!! Form::select('category_id',[$categoriesList], null,['class' => 'form-control ']) !!}



                           </div>
                           <div class="form-group">
                               <label class="form-label" for="full-name">Course Title</label>

                               {!! Form::text('title',null, ['class' => 'form-control ', 'placeholder'=>'Enter Category Title','required' =>'required']) !!}

                           </div>
                           <div class="form-group">
                               <label class="form-label" for="full-name">Course Short Description</label>

                               {!! Form::textarea('short_description',null, ['class' => 'form-control ','rows' =>'2', 'placeholder'=>'Course Short Description','required' =>'required']) !!}

                           </div>
                           <div class="form-group">
                               <label class="form-label" for="full-name">Course Description</label>

                               {!! Form::textarea('description',null, ['class' => 'form-control ','rows' =>'2', 'placeholder'=>'Course Description','required' =>'required']) !!}

                           </div>
                           <div class="form-group">
                               <label class="form-label" for="full-name">Course Outcomes</label>

                               {!! Form::textarea('outcomes',null, ['class' => 'form-control','rows' =>'2', 'placeholder'=>'Course Outcomes','required' =>'required']) !!}

                           </div>
                           <div class="form-group">
                               <label class="form-label" for="full-name">Course Section</label>

                               {!! Form::textarea('section',null, ['class' => 'form-control ','rows' =>'2', 'placeholder'=>'Course Section','required' =>'required']) !!}

                           </div>
                           <div class="form-group">
                               <label class="form-label" for="full-name">Course Requirements</label>

                               {!! Form::textarea('requirements',null, ['class' => 'form-control ','rows' =>'2', 'placeholder'=>'Course Requirements','required' =>'required']) !!}

                           </div>

                           <div class="form-group">
                               <label class="form-label" for="full-name">Course Language</label>

                               {!! Form::select('language',['English'=>'English','Malayalam'=>'Malayalam','Tamil'=>'Tamil','Kannada'=>'Kannada'],null, ['class' => 'form-control ','required' =>'required']) !!}

                           </div>

                           <div class="form-group">
                               <label class="form-label" for="full-name">Course Price</label>

                               {!! Form::text('price',null, ['class' => 'form-control ', 'placeholder'=>'Enter Price','required' =>'required']) !!}

                           </div>
                           <div class="form-group">
                               <label class="form-label" for="full-name">Course  Level</label>
                               {!! Form::select('level',['Beginner'=>'Beginner','Intermediate'=>'Intermediate','Expert'=>'Expert'], null,['class' => 'form-control ']) !!}


                           </div>
                           <div class="form-group">
                               <label class="form-label">Course Status</label>
                               {!! Form::select('visibility',['0'=>'Active','1'=>'InActive'], null,['class' => 'form-control ']) !!}


                           </div>
                           <div class="form-group">
                               <label class="form-label">Course thumbnail</label>
                               <input type="file" id="thumbnail" name="thumbnail">


                           </div>
                           <div class="form-group">
                               <label class="form-label">Course Video URL</label>
                               {!! Form::text('video_url',null, ['class' => 'form-control ','required' =>'required']) !!}


                           </div>




                           <button type="reset" class="btn btn-secondary " >Reset</button>
                           <button type="submit" class="btn btn-primary" >Save</button>



                       </div>
                       {!! Form::close() !!}
                   </div>





               </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
