<?php

namespace App\Http\Controllers;
use App\Category;
use App\Course;
use App\Lesson;
use Illuminate\Http\Request;



class LessonController extends Controller
{
    public function index()
    {
        $data = [
            'page_title' => 'Manage Lessons'
        ];

        $lessons = Lesson::orderBy('created_at', 'desc')->take(10)->get();

        return view('dashboard.lessons.index', compact('lessons'),$data);
    }


    public function create()
    {
        $courseList =  Course::pluck('title','id');
        $data = [
            'lesson' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/courses/lessons',
            'page_title' => 'Add a New Lesson'
        ];

        return view('dashboard.lessons.edit',compact('courseList'),$data );
    }



    public function edit( $id)
    {

        $lesson = Lesson::findOrFail($id);
        $courseList =  Course::pluck('title','id');
        $data = [
            'lesson' => $lesson,
            'formMethod' => 'PUT',
            'url' => 'dashboard/courses/lessons/'.$id,
            'page_title' => 'Edit Lessons'
        ];


        return view('dashboard.lessons.edit',compact('courseList'),$data );
    }

    public function store(Request $request)
    {


        $lesson = new Lesson();
        $lesson->course_id          = $request->get('course_id');
        $lesson->title              = $request->get('title');
        $lesson->duration           = $request->get('duration');
        $lesson->video              = $request->get('video');
        $lesson->save();

        return redirect('dashboard/courses/lessons/'.$lesson->id.'/edit')->with('success', 'Lesson Created Successfully!');
    }


    public function update(Request $request, $id)
    {

        $lesson = Lesson::findOrFail($id);
        $lesson->course_id          = $request->get('course_id');
        $lesson->title              = $request->get('title');
        $lesson->duration           = $request->get('duration');
        $lesson->video              = $request->get('video');

        $lesson->save();

        return redirect('dashboard/courses/lessons/'.$lesson->id.'/edit')->with('success', 'Updated Created Successfully!');
    }
}
