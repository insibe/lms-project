<?php

namespace App\Http\Controllers;

use App\Category;
use App\Enroll;
use Cart;
use Illuminate\Http\Request;

class EnrollController extends Controller
{


    public function index()
    {
        $data = [
            'page_title' => 'Manage Enrollments'
        ];

        $enrollments = Enroll::orderBy('created_at', 'desc')->take(10)->get();

        return view('dashboard.enrollments.index', compact('enrollments'),$data);
    }





    public function enroll(Request $request)
    {
        foreach (Cart::getContent() as $cart) {
            auth()->user()->enrolls()->create([
                'course_id' => $cart->id
            ]);
        }

        Cart::clear();

        return redirect()->route('home');
    }
}
