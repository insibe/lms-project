<?php

namespace App\Http\Controllers;

use App\Category;
use App\Course;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CourseController extends Controller
{

    public function search(Request $request){
        // Get the search value from the request
        $search = $request->input('search');

        // Search in the title and body columns from the posts table
        $courses = Course::query()
            ->where('title', 'LIKE', "%{$search}%")
            ->get();


        // Return the search view with the resluts compacted
        return view('search', compact('courses'));
    }


    public function index()
    {
        $data = [
            'page_title' => 'Manage Courses'
        ];

        $courses = Course::orderBy('created_at', 'desc')->paginate(10);

        return view('dashboard.courses.index', compact('courses'),$data);
    }


    public function create()
    {

        $categoriesList =  Category::pluck('title','id');



        $data = [
            'course' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/courses/',
            'page_title' => 'Add a New Course'
        ];

        return view('dashboard.courses.edit',compact('categoriesList'),$data );
    }


    public function edit( $id)
    {

        $course = Course::findOrFail($id);

        $categoriesList =  Category::pluck('title','id');

        $data = [
            'course' => $course,
            'formMethod' => 'PUT',
            'url' => 'dashboard/courses/'.$id,
            'page_title' => 'Edit Course'
        ];


        return view('dashboard.courses.edit',compact('categoriesList'),$data );
    }

    public function store(Request $request)
    {

        $destination    = 'images/course/'; // image file upload path
        $saveFlag       = 0;

        if ($request->hasFile('thumbnail')) {
            $file       = $request->file('thumbnail');
            $extension  = $file->getClientOriginalExtension(); // getting image extension
            $fileName   = str_replace(" ","-",strtolower($request->get('title'))).'_'.'icon'.'_'.time().'.'.$extension; // renameing image
            Storage::putFileAs($destination,$file, $fileName);
            $thumbnailfileName   = $destination.$fileName;//file name for saving to db

        }
        $course = new Course();
        $course->title                      = $request->get('title');
        $course->short_description          = $request->get('short_description');
        $course->description                = $request->get('description');
        $course->outcomes                   = $request->get('outcomes');
        $course->section                    = $request->get('section');
        $course->requirements               = $request->get('requirements');
        $course->language                   = $request->get('language');
        $course->level                      = $request->get('level');
        $course->price                      = $request->get('price');
        $course->video_url                  = $request->get('video_url');
        $course->category_id                = $request->get('category_id');
        $course->visibility                 = $request->get('visibility');
        $course->thumbnail                  =  $thumbnailfileName ?? null;
        $course->save();

        return redirect('dashboard/courses/'.$course->id.'/edit')->with('success', 'Courses Created Successfully!');
    }


    public function update(Request $request, $id)
    {

        $destination    = 'images/course/'; // image file upload path
        $saveFlag       = 0;

        if ($request->hasFile('thumbnail')) {
            $file       = $request->file('thumbnail');
            $extension  = $file->getClientOriginalExtension(); // getting image extension
            $fileName   = str_replace(" ","-",strtolower($request->get('title'))).'_'.'icon'.'_'.time().'.'.$extension; // renameing image
            Storage::putFileAs($destination,$file, $fileName);
            $thumbnailfileName   = $destination.$fileName;//file name for saving to db

        }

        $course = Course::findOrFail($id);


        $course->title                      = $request->get('title');
        $course->short_description          = $request->get('short_description');
        $course->description                = $request->get('description');
        $course->outcomes                   = $request->get('outcomes');
        $course->section                    = $request->get('section');
        $course->requirements               = $request->get('requirements');
        $course->language                   = $request->get('language');
        $course->level                      = $request->get('level');
        $course->price                      = $request->get('price');
        $course->video_url                  = $request->get('video_url');
        $course->category_id                = $request->get('category_id');
        $course->visibility                 = $request->get('visibility');
        $course->thumbnail                  =  $thumbnailfileName ?? null;

        $course->save();

        return redirect('dashboard/courses/'.$course->id.'/edit')->with('success', 'Course Updated Successfully!');
    }

    public function destroy($id)
    {
        $course = Course::find($id);
        $course->delete();
        return redirect('dashboard/courses/')->with('success', 'Course Deleted Successfully!');

    }
}
