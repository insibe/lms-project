<?php

namespace App\Http\Controllers;

use App\Category;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {

        $data = [
            'page_title' => 'Manage Categories'
        ];

        $categories = Category::orderBy('created_at', 'desc')->paginate(10);
        return view('dashboard.categories.index', compact('categories'),$data);
    }


    public function create()
    {

        $data = [
            'category' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/categories/',
            'page_title' => 'Add a New Category'
        ];

        return view('dashboard.categories.edit',$data );
    }


    public function edit( $id)
    {

        $category = Category::findOrFail($id);
        $data = [
            'category' => $category,
            'formMethod' => 'PUT',
            'url' => 'dashboard/categories/'.$id,
            'page_title' => 'Edit Category'
        ];


        return view('dashboard.categories.edit',$data );
    }

    public function store(Request $request)
    {
        $category = new Category();
        $category->title   = $request->get('title');
        $category->slug   =str_slug($category->title , "-").'-'.time();

        $category->save();

        return redirect('dashboard/categories/'.$category->id.'/edit')->with('success', 'Category Created Successfully!');
    }


    public function update(Request $request, $id)
    {

        $category = Category::findOrFail($id);


        $category->title  = $request->get('title');
        $category->slug   =str_slug($category->title , "-").'-'.time();

        $category->save();

        return redirect('dashboard/categories/'.$category->id.'/edit')->with('success', 'Updated Created Successfully!');
    }


    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect('dashboard/categories/')->with('success', 'Category Deleted Successfully!');

    }
}
